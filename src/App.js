import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Header from './comopnents/ui/Header'
import MovieGrid from './comopnents/movies/MovieGrid'
import Search from './comopnents/ui/Search'

import './App.css'

const App = ( ) => {
  const [items, setItems] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  const [query, setQuery] = useState('')

  useEffect(() => {
    const fetchItems = async () => {
      setIsLoading(true)
      const result = await axios(`https://swapi.dev/api/films/?format=json&name=${query}`)

      console.log(result.data.results)
      setItems(result.data.results)
      setIsLoading(false)
    }

    fetchItems()
  }, [query])

  return (
    <div className="container">
     <Header/>
     <Search getQuery={(q) => setQuery(q) }/>
     <MovieGrid isLoading={isLoading} items={items}/>
    </div>
  )
}

export default App
