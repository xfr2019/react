import React from "react"
import MovieItem from './MovieItem'


const MovieGrid = ({ items, isLoading }) => {
  return isLoading ? (
    <h1> Loading ... </h1>
  ) : (
    <section className="cards">
      {items.map((item) => (
        <>
          <div className="card" data-key={item.episode_id}>
            <MovieItem key={item.episode_id} item={item}></MovieItem>
          </div>
        </>
      ))}
    </section>
  );
};

export default MovieGrid;
