import React from 'react'

const CharacterItem = ({ item }) => {
    console.log(item);
    return (
        <div>
            <h3 className="card__title"><a href={item.url}>{item.title}</a> </h3>
            <p className="card__description">{item.opening_crawl}</p>
            <p> <b>director:</b> {item.director} </p>
            <p> <b>producer:</b> {item.producer} </p>
            <p> <b>release date:</b> {item.release_date} </p>
            <p>
              <a href={item.url}> demo </a>
            </p>
        </div>
    )
}

export default CharacterItem