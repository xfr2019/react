import React, { useState } from "react"

const Search = ({ getQuery }) => {
  const [text, setText] = useState("")
  const onChange = (q) => {
    setText(q);
    getQuery(q);
  };

  return (
    <div className="search">
      <form>
        <input
          type="search"
          className="form-control"
          placeholder="Search film..."
          value={text}
          onChange={(e) => onChange(e.target.value)}
          autoFocus
        />
      </form>
    </div>
  );
};
export default Search
